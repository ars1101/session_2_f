import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:session2_final/home/repository/supabase.dart';
import 'package:session2_final/main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  Map<String, dynamic> elem = {};

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {setState(() async {
      elem = await getUser();
    }); });
  }
  bool obs = true;
  bool check = false;
  int current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Image.asset('assets/ken.png'),
              SizedBox(width: 25,),
              Text(elem['fullname'], style: TextStyle(fontSize: 24),),
              SizedBox(width: 25,),
              Text(elem['balance'].toString(), style: TextStyle(fontSize: 24),),
              SizedBox(
                width: 5,
              ),
              Column(
                children: [],
              )
            ],
          ),
          SizedBox(
            height: 64,
          ),
          ListTile(
            leading: Image.asset('assets/prof.png'),
            title: Text('Edit Profile'),
            subtitle: Text('Name, phone no, address, email ...'),
          ),
          SizedBox(height: 12,),
          ListTile(
            leading: Image.asset('assets/stat.png'),
            title: Text('Statements & Reports'),
            subtitle: Text('Download transaction details, orders, deliveries'),
          ),
          SizedBox(height: 12,),
          ListTile(
            leading: Image.asset('assets/not.png'),
            title: Text('Notification Settings'),
            subtitle: Text('mute, unmute, set location & tracking setting'),
          ),
          SizedBox(height: 12,),
          ListTile(
            leading: Image.asset('assets/card.png'),
            title: Text('Card & Bank account settings'),
            subtitle: Text('change cards, delete card details'),
          ),
          SizedBox(height: 12,),
          ListTile(
            leading: Image.asset('assets/ref.png'),
            title: Text('Referrals'),
            subtitle: Text('check no of friends and earn'),
          ),
          SizedBox(height: 12,),
          ListTile(
            leading: Image.asset('assets/map.png'),
            title: Text('About Us'),
            subtitle: Text('check no of friends and earn'),
          ),
          SizedBox(height: 12,),
          GestureDetector(onTap: (){supabase.auth.signOut();
            user = null;
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => SignIn()));
            },
            child: SizedBox(
              child: ListTile(
                leading: Image.asset('assets/logout.png'),
                title: Text('Log Out'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

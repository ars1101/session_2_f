import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:session2_final/home/presentation/pages/Profile.dart';
import 'package:session2_final/home/presentation/pages/send.dart';
import 'package:session2_final/main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool obs = true;
  bool check = false;
  int current = 0;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(items: [
      BottomNavigationBarItem(icon: Image.asset('assets/house-2.png',), label: 'Home'),
        BottomNavigationBarItem(icon: Image.asset('assets/wallet-3.png',), label: 'Wallet'),
        BottomNavigationBarItem(icon: Image.asset('assets/smart-car.png',), label: 'Track'),
        BottomNavigationBarItem(icon: Image.asset('assets/profile-circle.png',), label: 'Profile')
      ],currentIndex: current, onTap: (t){setState(() {
        current = t;
      });}, ),
      body: [Center(child: FilledButton(child: Text('To Order'), onPressed: (){
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => Send()));
      },),),
        SizedBox(),
        SizedBox(),
        Profile()
      ][current],

    );
  }
}

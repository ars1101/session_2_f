import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:session2_final/home/presentation/pages/Profile.dart';
import 'package:session2_final/main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class Send extends StatefulWidget {
  const Send({super.key});

  @override
  State<Send> createState() => _SendState();
}

class _SendState extends State<Send> {
  bool obs = true;
  bool check = false;
  int current = 0;
  int count = 1;
  TextEditingController address = TextEditingController();
  TextEditingController state = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController others = TextEditingController();
  TextEditingController items = TextEditingController();
  TextEditingController weight = TextEditingController();
  TextEditingController worth = TextEditingController();
  List<String> dest = [' ', ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Text('Origin Detaild'),
                TextField(
                    controller: address,
                    decoration: InputDecoration(hintText: 'Address')),
                SizedBox(
                  height: 5,
                ),
                TextField(
                    controller: state,
                    decoration: InputDecoration(hintText: 'State,Country')),
                SizedBox(height: 5),
                TextField(
                    controller: phone,
                    decoration: InputDecoration(hintText: 'Phone number')),
                SizedBox(height: 5),
                TextField(
                    controller: weight,
                    decoration: InputDecoration(hintText: 'Others')),
                SizedBox(height: 5),
                TextField(
                    controller: items,
                    decoration: InputDecoration(hintText: 'package items')),
                SizedBox(height: 5),
                TextField(
                    controller: weight,
                    decoration:
                        InputDecoration(hintText: 'Weight of item(kg)')),
                SizedBox(height: 5),
                TextField(
                  controller: worth,
                  decoration: InputDecoration(hintText: 'worth of items'),
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
          SliverList.builder(
            itemBuilder: (_, ind) {
              String adr = '';
              String state = '';
              String phone = '';
              String oth = '';
              return Column(
                children: [
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: TextField(
                      decoration: InputDecoration(hintText: 'Address'),
                      onChanged: (tx) {
                        setState(
                          () {
                            adr = tx;
                            print(adr);
                            dest[ind] = ((ind + 1).toString() + '.' + adr + state + phone);
                            print(dest);
                          },
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextField(
                      decoration: InputDecoration(hintText: 'State,Country'),
                      onChanged: (tx) {
                        setState(() {
                          state = tx;
                          dest[ind] = (ind + 1).toString() +
                              '.' +
                              adr +
                              ' ' +
                              state +
                              ' ' +
                              phone;
                        });
                      }),
                  SizedBox(height: 5),
                  TextField(
                    onChanged: (tx) {
                      setState(() {
                        phone = tx;
                        dest[0] = (ind + 1).toString() +
                            '.' +
                            adr +
                            ' ' +
                            state +
                            ' ' +
                            phone;
                      });
                    },
                    decoration: InputDecoration(hintText: 'Phone number'),
                  ),
                  SizedBox(height: 5),
                  TextField(
                      onChanged: (tx) {
                        setState(() {
                          oth = tx;
                          dest[ind] = (ind+1).toString() + '. ' + adr + state + phone;
                          print(dest);
                        });
                      },
                      decoration: InputDecoration(hintText: 'Others')),
                  SizedBox(height: 5),
                ],
              );
            },
            itemCount: count,
          ),
          SliverToBoxAdapter(
            child: Column(
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        count = count + 1;
                        dest.add('');
                        print(dest);
                      });
                    },
                    icon: Icon(Icons.add))
              ],
            ),
          )
        ],
      ),
    );
  }
}

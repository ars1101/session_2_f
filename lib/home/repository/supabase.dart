import 'package:session2_final/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<Map<String, dynamic>> getUser()async {
  var id = user!.id;
  var elem = await supabase.from('profiles').select().eq('id_user', id).single();
  print(elem);
  return elem;
}
import 'package:flutter/material.dart';
import 'package:session2_final/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


Future<User?> regIn(name,password,email,phone) async {
  var res = await supabase.auth.signUp(password: password, email: email);
  User? user = res.user;
  await supabase.from('profiles').insert({
    'fullname': name,
    'phone' : phone,
    'avatar' : '',
    'id_user' : user!.id,
  });
  return user;
}

Future<AuthResponse?> logIn(password,email) async {
  var res = await supabase.auth.signInWithPassword(
      password: password, email: email);
  return res;
}

Future<void> sendOtp(email) async {
  await supabase.auth.resetPasswordForEmail(email);
}

Future<User?> Verifyotp(String otp, String email) async {
  var res = await supabase.auth
      .verifyOTP(token: otp, type: OtpType.email, email: email);
  var user = res.user;
  return user;
}

Future<void> newpassword(newpass) async {
  await supabase.auth.updateUser(UserAttributes(password: newpass));
}
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:session2_final/home/presentation/pages/home.dart';
import 'package:session2_final/main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController passc = TextEditingController();
  bool obs = true;
  bool check = false;
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');

  bool isValid() {
    if ((name.text.length > 0) &
        (phone.text.length > 0) &
        (pass.text.length > 6) &
        (passc.text == pass.text) & EmailValidator.validate(email.text)& (pass.text == passc.text)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 76,
                ),
                Text(
                  'Create an account',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Complete the sign up process to get started',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 33,
                ),
                SizedBox(
                  height: 72,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomTextField(onChanged: (s) {
                    setState(() {});
                  },
                      label: 'Full Name',
                      hint: 'Ivanov Ivan',
                      obs: false,
                      ctr: name),
                ),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 72,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomTextField(
                    label: 'Phone Number',
                    hint: '+7(999)999-99-99',
                    obs: false,
                    ctr: phone,
                    formatter: MaskTextInputFormatter(mask: '+#(###)###-##-##'),
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField(onChanged: (s) {
                      setState(() {});
                    },
                        label: 'Email Address',
                        hint: '***********@mail.com',
                        obs: false,
                        ctr: email)),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField(onChanged: (s) {
                      setState(() {});
                    },
                      label: 'Password',
                      hint: '**********',
                      obs: obs,
                      ctr: pass,
                      onTapSuffix: () {
                        setState(() {
                          if (obs) {
                            obs = false;
                          } else {
                            obs = true;
                          }
                        });
                      },
                    )),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField(
                      onChanged: (s) {
                        setState(() {});
                      },
                      label: 'Confirm Password',
                      hint: '**********',
                      obs: obs,
                      ctr: passc,
                      onTapSuffix: () {
                        setState(() {
                          if (obs) {
                            obs = false;
                          } else {
                            obs = true;
                          }
                        });
                      },
                    )),
                SizedBox(
                  height: 37,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 1,
                      ),
                      SizedBox(
                        height: 14,
                        width: 14,
                        child: Checkbox(
                            value: check,
                            side: BorderSide(
                                color: Color.fromARGB(255, 0, 108, 236),
                                width: 1),
                            activeColor: Color.fromARGB(255, 0, 108, 236),
                            onChanged: (bool? val) {
                              setState(() {
                                check = val!;
                              });
                            }),
                      ),
                      SizedBox(
                        width: 26,
                      ),
                      GestureDetector(
                        onTap: () async {
                          await launchUrl(pdf);
                        },
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              text: "By ticking this box, you agree to our ",
                              style: Theme.of(context).textTheme.titleSmall,
                              children: [
                                TextSpan(
                                    text:
                                        "Terms and \nconditions and private policy",
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 235, 188, 46)))
                              ],
                            )),
                      ),
                    ]),
                SizedBox(
                  height: 64,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  height: 46,
                  child: FilledButton(
                    child: Text(
                      'Sign Up',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: ((name.text.length > 0) &
                    (phone.text.length > 0) & (pass.text.length > 0) &
                    (passc.text == pass.text) & EmailValidator.validate(email.text)& (pass.text == passc.text))? () async {
                      try{
                      var res = await regIn(name.text, pass.text, email.text, phone.text);
                      if(res != null){user = res;
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Home()));}
                      }on AuthException catch(e){
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
                      }

                    } : null,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SignIn()));
                        },
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: Color(0xFF0560FA),

                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 18,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'or sign in using',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/google.png'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 28,
                )
              ],
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}

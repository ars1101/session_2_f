import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:pinput/pinput.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/pages/signup.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:session2_final/main.dart';

class newpass extends StatefulWidget {
  const newpass({super.key,});

  @override
  State<newpass> createState() => _newpassState();
}

class _newpassState extends State<newpass> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController pass = TextEditingController();
  TextEditingController passc = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if (pass.text.length >= 6) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'New Password',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Enter new passwordg',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 33,
                ),
        SizedBox(
            height: 72,
            width: MediaQuery.of(context).size.width - 48,
            child: CustomTextField(onChanged: (s) {
              setState(() {});
            },
              label: 'Password',
              hint: '**********',
              obs: obs,
              ctr: pass,
              onTapSuffix: () {
                setState(() {
                  if (obs) {
                    obs = false;
                  } else {
                    obs = true;
                  }
                });
              },
            )),
        SizedBox(
          height: 24,
        ),
        SizedBox(
            height: 72,
            width: MediaQuery.of(context).size.width - 48,
            child: CustomTextField(
              onChanged: (s) {
                setState(() {});
              },
              label: 'Confirm Password',
              hint: '**********',
              obs: obs,
              ctr: passc,
              onTapSuffix: () {
                setState(() {
                  if (obs) {
                    obs = false;
                  } else {
                    obs = true;
                  }
                });
              },
            )),
                SizedBox(
                  height: 71,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (!isValid())
                        ? null
                        : () async {
                            try {
                              await newpassword(pass.text);
                              supabase.auth.signOut();
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (context) => Holder()));

                            } on AuthException catch (e) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text(e.message)));
                            }
                          },
                    child: Text('Log in'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 28,
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}

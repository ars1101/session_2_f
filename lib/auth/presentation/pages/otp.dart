import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:pinput/pinput.dart';
import 'package:session2_final/auth/presentation/pages/forgotpass.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/newpass.dart';
import 'package:session2_final/auth/presentation/pages/signin.dart';
import 'package:session2_final/auth/presentation/pages/signup.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:session2_final/main.dart';

class otp extends StatefulWidget {
  final String email;
  const otp({super.key, required this.email});

  @override
  State<otp> createState() => _otpState();
}


class _otpState extends State<otp> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController otp = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {

    if (otp.text.length == 6) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'OTP Verification',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Enter the 6 digit numbers sent to your email',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 33,
                ),

                SizedBox(
                    height: 32,
                    width: MediaQuery.of(context).size.width - 48,
                    child: Pinput(length: 6, controller: otp,
                    defaultPinTheme: PinTheme(width: 32,height: 32,decoration: BoxDecoration(borderRadius: BorderRadius.zero, border: Border.all(color: Color(0xFFA7A7A7)))),
                      separatorBuilder: (i){return SizedBox(width: 30,);}, submittedPinTheme: PinTheme(width: 32,height: 32,decoration: BoxDecoration(borderRadius: BorderRadius.zero, border: Border.all(color: Color(0xFF0560FA)))),

                    )),
                SizedBox(height: 12,),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('If you didn’t receive code, resend after 1:00', style: Theme.of(context).textTheme.titleSmall,),
                    ],
                  ),
                ),
                SizedBox(
                  height: 84,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (!isValid()) ? null : () async {try{
                      user = await Verifyotp(otp.text, emaile.text);
                      if(user != null){
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => newpass()));}
                    } on AuthException catch(e){
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
                    }
                    },
                    child: Text('Set New Password'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),

                SizedBox(height: 28,)
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}


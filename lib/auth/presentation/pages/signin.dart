import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session2_final/auth/presentation/pages/forgotpass.dart';
import 'package:session2_final/auth/presentation/pages/holder.dart';
import 'package:session2_final/auth/presentation/pages/signup.dart';
import 'package:session2_final/auth/presentation/widgets/customtextfield.dart';
import 'package:session2_final/auth/repository/supawidgets.dart';
import 'package:session2_final/home/presentation/pages/home.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:session2_final/main.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}


class _SignInState extends State<SignIn> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if (EmailValidator.validate(email.text) &
    (email.text.length > 0) &
    (pass.text.length >= 6)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'Welcome Back',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Fill in your email and password to continue',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 33,
                ),

                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChanged: (te){setState(() {});},
                        label: 'Email Address',
                        hint: '***********@mail.com',
                        obs: false,
                        ctr: email)),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChanged: (te){setState(() {});},
                        label: 'Password',
                        hint: '**********',
                        obs: obs,
                        ctr: pass,
                        onTapSuffix: () {
                          setState(() {
                            if (obs == true) {
                              obs = false;
                            } else {
                              obs = true;
                            }
                          });
                        })),
                SizedBox(
                  height: 37,
                ),
                Row(
                  children: [
                    SizedBox(
                      height: 14,
                      width: 14,
                      child: Checkbox(
                          value: check,
                          onChanged: (val) {
                            setState(() {
                              check = val!;
                            });
                          }),
                    ),
                    SizedBox(
                      width: 11,
                    ),Text(
                      'Remember Password',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Color(0xFFA7A7A7)),
                    ), SizedBox(width: 70,),

                    GestureDetector( onTap: (){
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => ForgotPass()));

                      },
                      child: Text(
                        'Forgot password',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFF0560FA)),
                      ),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.start,
                ),
                SizedBox(
                  height: 64,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (!isValid()) ? null : () async {
                      try{
                      var res = await logIn(pass.text, email.text,);
                      if(res != null){user = res.user!;
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Home()));
                      print(user);
                      }
                    } on AuthException catch(e){
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
                    }
                    },
                    child: Text('Log In'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => SignUp()));
                        },
                        child: Text(
                          'Sign Up',
                          style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: Color(0xFF0560FA),

                        ),
                        ),
                      )
                    ],
                  ),

                ),
                SizedBox(height: 18,),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'or log in using',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),],
                  ),


                ), SizedBox(height: 8,),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Image.asset('assets/google.png'),],
                  ),


                ),
                SizedBox(height: 28,)
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}

